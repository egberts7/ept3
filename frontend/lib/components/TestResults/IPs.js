import React, { Component } from 'react';

import css from 'next/css';

const style = {
  root: css({
    margin: '0.3em 1em 0 1em',
  }),
  list: css({
    listStyleType: 'none',
    margin:        0,
    padding:       0,
  }),
};

export default class TestResultsIPs extends Component {

  render() {
    const { lang } = this.props;

    const ips = this.ips();
    if (!ips.length) return null;

    return (
      <div className={ style.root }>
        {
          lang === 'es' ? (
            <h3>Direcciones&#160;IP</h3>
          ) : (
            <h3>IP&#160;Addresses</h3>
          )
        }
        <ul className={ style.list }>
          {
            ips.map(ip => (
              <li key={ ip }>
                { ip }
              </li>
            ))
          }
        </ul>
      </div>
    );

  }

  ips() {

    const { results } = this.props;
    const ips = {};
    results.forEach(({ ip }) => ips[ip] = true);
    return Object.keys(ips).sort();

  }

}
