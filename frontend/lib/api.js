import 'isomorphic-fetch';

import ServerDate from './ServerDate';

import { backendUrl, backendUrlInternal } from '../config';

const apiUrl = typeof window === 'undefined' ? (backendUrlInternal || backendUrl) : backendUrl;

async function callApi(url, options) {

  const res = await fetch(url, options);

  try {
    const dateHeader = res.headers.get('date');
    if (dateHeader) {
      const serverDate = new Date(dateHeader).getTime();
      const clientDate = Date.now();
      ServerDate.updateDiff(clientDate - serverDate);
    }
  } catch (err) {
    // A failure to get this info should not crash things
  }

  if (!res.ok) throw new Error(`${res.status} ${res.statusText}`);

  const json = await res.json();

  if (json.error) throw new Error(json.error);

  return json.ok;

}

export default {
  post (endPoint, params) {
    return callApi(`${apiUrl}/${endPoint}`, {
      method: 'POST',
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });
  },
  get (endPoint, params = {}) {

    params['__t'] = Date.now();
    params['__r'] = Math.random();

    const qs = Object.keys(params).map(k => (
      `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
    )).join('&');

    let url = `${apiUrl}/${endPoint}`;
    if (qs.length) url += `?${qs}`;

    return callApi(url);
  },
};
