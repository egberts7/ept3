import mongoose from '../mongo';

import { expireTests } from '../../../config';

const Schema = new mongoose.Schema({
  email: String,
  submit: {
    ip: {
      client:           String,
      httpForwardedFor: String,
    },
    time: { type: Date, default: () => new Date() },
  },
  accessed: {
    type:    Boolean,
    default: false,
  },
  testEmailsSent: {
    type:    Number,
    default: 0,
  },
});

Schema.index(
  { 'submit.time': 1 },
  { expireAfterSeconds: parseInt(expireTests / 1000, 10) }
);

export default mongoose.model('tests', Schema);
