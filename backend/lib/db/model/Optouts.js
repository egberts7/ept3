import mongoose from '../mongo';

const Schema = new mongoose.Schema({
  email: String,
  ip: {
    client:           String,
    httpForwardedFor: String,
  },
  time: { type: Date, default: () => new Date() },
});

export default mongoose.model('optouts', Schema);
