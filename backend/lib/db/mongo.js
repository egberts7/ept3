import mongoose from 'mongoose';
import { mongoUrl } from '../../config';

mongoose.Promise = global.Promise;

export default mongoose.connect(mongoUrl);
