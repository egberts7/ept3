import { longPollTimeout } from '../config';

const callbacks = {};

export function newCallback (code) {
  if (code in callbacks) {
    callbacks[code].forEach(c => {
      clearTimeout(c.pid);
      c.callback();
    });
    delete callbacks[code];
  }
}

export function onCallback (code, callback) {
  callbacks[ code ] = callbacks[ code ] || [];
  callbacks[ code ].push({
    callback,
    pid: setTimeout(() => {
      callbacks[ code ] = callbacks[ code ].filter(c => c.callback !== callback);
      callback();
    }, longPollTimeout),
  });
}

export function sleepUntilCallback (code) {
  return new Promise(resolve => onCallback(code, resolve));
}
