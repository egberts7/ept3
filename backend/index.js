import express    from 'express';
import bodyParser from 'body-parser';

import callbackRoute      from './routes/callback';
import deleteResultsRoute from './routes/deleteResults';
import optoutRoute        from './routes/optout';
import sendConfirmRoute   from './routes/sendConfirm';
import sendTestRoute      from './routes/sendTest';
import testRoute          from './routes/test';
import testListRoute      from './routes/testList';
import testResultsRoute   from './routes/testResults';

import { backendUrl, frontendUrl } from './config';

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
 * CORS middleware
 */
app.use((req, res, next) => {

  const { origin } = req.headers;
  if (origin === frontendUrl) {

    res.header('Access-Control-Allow-Origin',  origin);
    res.header('Access-Control-Allow-Headers', 'content-type');
    res.header('Access-Control-Expose-Headers', 'date');
  }
  next();

});

/**
 * Add useful "remote" middleware
 */
app.use((req, res, next) => {

  req.remote = () => {
    const ip = {
      client: req.ip.replace(/^::ffff:/i, ''),
    };

    const ff = (req.headers['x-forwarded-for'] || '')
      .split(/[\s,]+/)
      .filter(ip => ip.length);

    if (ff.length) {
      if (ip.client.match(/^(127\..+|192\.168\..+|10\..+|172\.(1[6789]|2[0-9]|3[01])\..+)$/)) {
        ip.client = ff.pop();
      }
      if (ff.length) ip.httpForwardedFor = ff.join(', ');
    }

    return ip;
  };
  next();

});

app.post('*', (req, res, next) => {

  const contentType = req.headers['content-type'];
  if (contentType === 'application/json') return next();
  next(`Bad content type: ${contentType}`);

});

app.get('/callback',       callbackRoute);
app.post('/deleteResults', deleteResultsRoute);
app.post('/optout',        optoutRoute);
app.post('/sendConfirm',   sendConfirmRoute);
app.post('/sendTest',      sendTestRoute);
app.get('/test',           testRoute);
app.get('/testList',       testListRoute);
app.get('/testResults',    testResultsRoute);

(() => {

  const port = process.env.PORT;

  app.listen(port, () => {

    console.log(`Listening on port ${port}`); // eslint-disable-line no-console

  });

})();
