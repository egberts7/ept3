import React from 'react';
import svg   from './svg';
import svg2  from './svg2';

/**
 * Helper function to hex encode a string. Like the standard escape function
 * but does it to *every* character
 */
function hexEncode(str) {
  let ret = '';
  let c = str.length;
  while (c) {
    let hexChar = str.charCodeAt(--c).toString(16);
    if (hexChar.length < 2) hexChar = '0' + hexChar;
    ret = '%' + hexChar + ret;
  }
  return ret;
}

const jsm = 'XSS(1) found. Please report to xss@emailprivacytester.com';
const js1 = `alert(unescape(/${hexEncode(jsm)}/.source))`;
const js2 = `<script>alert(unescape(/${hexEncode(jsm.replace('(1)','(2)'))}/.source))</script>`;
const js3 = `alert(unescape(/${hexEncode(jsm.replace('(1)','(3)'))}/.source))`;

const attachments = [
  {
    filename:           '%%CODE%%.css',
    content:            '* { background-image: url(\'%%BASE%%cssAttachment\'); }',
    cid:                '%%CODE%%.css@%%FRONTEND_DOMAIN%%',
    contentDisposition: 'inline',
    contentType:        'text/css',
  },
  {
    filename:           '%%CODE%%.svg',
    cid:                '%%CODE%%.svg@%%FRONTEND_DOMAIN%%',
    contentDisposition: 'inline',
    contentType:        'image/svg+xml',
    content:            svg,
  },
  {
    filename:           '%%CODE%%-2.svg',
    cid:                '%%CODE%%.svg2@%%FRONTEND_DOMAIN%%',
    contentDisposition: 'inline',
    contentType:        'image/svg+xml',
    content:            svg2,
  },
  {
    filename:           `${js2}.%%CODE%%.png`,
    contents:           js1,
    cid:                '%%CODE%%.js@%%FRONTEND_DOMAIN%%',
    contentDisposition: 'inline',
    contentType:        'image/png',
  },
];

const html = lang => ({ code, base, dnsTestZone, frontendDomain }) => {

  return (
    <html lang={ lang } manifest={ `${base}manifest` }>
      <head>

        <meta name="author"      content="Mike Cardwell. &#60;https://grepular.com/&#62;"/>
        <meta name="copyright"   content="Copyright ©2016 Mike Cardwell. All rights reserved."/>
        <meta name="description" content="Email Privacy Tester"/>

        <title>Email Privacy Tester</title>

        <link rel="alternate" type="application/rss+xml" href={ `${base}rss` }/>

        <link rel="alternate" type="application/atom+xml" href={ `${base}atom` }/>

        <script type="text/javascript" src={ `${base}js` }/>

        <link rel="stylesheet" type="text/css" href={ `${base}css` }/>

        <link rel="stylesheet" type="text/css" href={ `cid:${code}.css@${frontendDomain}` }/>

        <link rel="search" type="application/opensearchdescription+xml" href={ `${base}opensearch` }/>

        <meta http-equiv="Refresh" content={ `1; URL=${base}metaRefresh` }/>

      </head>
      <body background={ `${base}background` }>

        {/* Information */}

          <p style={{
            borderBottom:  '1px solid #000',
            paddingBottom: '1em',
          }}>
            <span style={{
              fontSize:     '1.4em',
              borderBottom: '1px solid #333',
            }}>Email Privacy Tester</span><br/>
            <br/>
            {
              lang === 'es'
                ? 'Este es su email de prueba del Probador de Privacidad de Email. Por favor, ignore todo lo que haya debajo de esta línea. Clicar en algo alterará los resultados.'
                : 'This is your test email from the Email Privacy Tester. Please ignore everything after this line. Clicking on anything will skew the results.'
            }
          </p>

        {/* CSS Test - background-image:url */}

          <p style={{
            backgroundImage: `url('${base}backgroundImage')`,
          }}/>

        {/* CSS Test - content:url */}

          <p style={{
            content: `url('${base}cssContent')`,
          }}/>

        {/* CSS Test - behavior:url */}

          <p style={{
            behavior: `url('${base}cssBehavior') url('${base}cssBehavior')`,
          }}/>

        {/* CSS Test - font-face src:url */}

          <style type="text/css" dangerouslySetInnerHtml={{ __html: `
            @font-face {
              src: url('${base}fontFace');
            }
          `}}/>

        {/* CSS Test - @import 'url' */}

          <style type="text/css" dangerouslySetInnerHtml={{ __html: `
            @import '${base}cssImport';
          `}}/>

        {/* CSS Test - Encoding CSS */}

          <div id="cssEscape"></div>
          <style type="text/css" dangerouslySetInnerHtml={{ __html: `
            #cssEscape {
              background: \\75 \\72 \\6C ('${base}cssEscape');
            }
          `}}/>

        {/* SVG background-image test */}

          <object
            type = "image/svg+xml"
            data = { `cid:${code}.svg@${frontendDomain}` }
          >
            <embed
              type = "image/svg+xml"
              src  = { `cid:${code}.svg@${frontendDomain}` }
              pluginspage = "http://www.adobe.com/svg/viewer/install/"
            />
          </object>
          <iframe
            src    = { `cid:${code}.svg@${frontendDomain}` } width  = "1"
            height = "1"
          />

        {/* SVG Inline Image */}

          <span dangerouslySetInnerHtml={{ __html: `
            <svg
              version     = "1.1"
              baseProfile = "full"
              xmlns       = "http://www.w3.org/2000/svg"
              xmlns:xlink = "http://www.w3.org/1999/xlink"
            >
              <image xlink:href="${base}svgInlineImage"/>
            </svg>
          `}}/>

        {/* IMG Test */}

          <img width="16" height="16" src={ `${base}img` }/>

        {/* IMG srcset attribute */}

          <img
            src    = "#"
            srcset = { `${base}/imgSrcset 1x` }
            width  = "16"
            height = "16"
          />

        {/* Image Submit */}

          <input type="image" src={ `${base}imageSubmit` }/>

        {/* Anchor DNS Prefetch Test */}

          <a href={ `http://${code}.anchor-test.${dnsTestZone}` }
          />

        {/* Link DNS Prefetch Test */}

          <link
            rel  = "dns-prefetch"
            href = { `http://${code}.link-test.${dnsTestZone}` }
          />

        {/* Link Prefetch Test */}

          <link rel="prefetch" href={ `${base}linkPrefetch` }/>

        {/* Video Tag Tests */}

          <video
            autoplay = "true"
            src      = { `${base}video` }
            width    = "1"
            height   = "1"
          ></video>
          <video
            poster   = { `${base}videoPoster` }
            autoplay = "true"
            width    = "1"
            height   = "1"
          >
            <source src={ `${base}videoMp4` }  type='video/mp4; codecs="avc1.4D401E, mp4a.40.2"'/>
            <source src={ `${base}videoWebm` } type='video/webm; codecs="vp8.0, vorbis"'/>
            <source src={ `${base}videoOgg` }  type='video/ogg; codecs="theora, vorbis"'/>
          </video>

        {/* Audio Tag Test */}

          <audio src={ `${base}audio` } preload="metadata" autoplay="autoplay" width="1" height="1"/>

        {/* Applet Tag Test */}

          <span dangerouslySetInnerHtml={{ __html: `
            <applet codebase="${base}applet" code="applet.class" width="1" height="1"></applet>
          `}}/>

        {/* Object Tag Test */}

          <object width="16" height="16" data={ `${base}objectData` } type="image/png"/>

        {/* Iframe Tag Test */}

          <iframe src={ `${base}iframe` } width="1" height="1"/>

        {/* BGSound Tag Test */}

          <span dangerouslySetInnerHtml={{ __html: `
            <bgsound src="${base}bgsound" loop="1" width="1" height="1"></bgsound>
          `}}/>

        {/* CSS Expression Test */}

          <p style={{
            backgroundColor: `expression((document.createElement('img'))['src']=${base}cssExpression)`,
          }}/>

        {/* Flash Test */}

          <object width="1" height="1">
             <param name="movie" value="flash.swf"/>
            <embed src={ `${base}flash` } width="1" height="1"/>
          </object>

        {/* View-Source Test */}

          <iframe width="1" height="1" src={ `view-source:${base}/viewSource` }/>

        {/* IFrame Meta Refresh Test */}

          <iframe width="1" height="1" src={ `data:text/html;charset=utf-8,&lt;html&gt;&lt;head&gt;&lt;meta http-equiv=&quot;Refresh&quot; content=&quot;1; URL${base}iframeRefresh&quot;&gt;&lt;/head&gt;&lt;body&gt;&lt;/body&gt;&lt;/html&gt;` }/>

        {/* IFrame Image Test */}

          <iframe width="1" height="1" border="none" src={ `data:text/html;charset=utf-8,&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;&lt;img src=&quot${base}iframeImg&quot;&gt;&lt;/body&gt;&lt;/html&gt;` }/>

        {/* Script in Script Test */}

          <span dangerouslySetInnerHtml={{ __html: `
            <sc<script></script>cript type="text/javascript" src="${base}scriptInScript">
            </sc<script></script>ript>
          ` }}/>

      </body>
    </html>
  );

};

export const en = {

  from: `"Email Privacy Tester <img src=x onerror='${js3}'>" <noreply@%%MAIL_SENDER_DOMAIN%%>`,

  subject: 'EPT - Your Test Email - %%FRONTEND_URL%%/test?code=%%CODE%%',

  list: {
    unsubscribe: {
      url:     '%%FRONTEND_URL%%/optout?code=%%CODE%%',
      comment: 'Opt out from further emails from %%FRONTEND_URL%%',
    },
  },

  attachments,

  html: html('en'),
};

export const es = {

  from: `"Email Privacy Tester <img src=x onerror='${js3}'>" <noreply@%%MAIL_SENDER_DOMAIN%%>`,

  subject: 'EPT - Su email de prueba - %%FRONTEND_URL%%/es/test?code=%%CODE%%',

  list: {
    unsubscribe: {
      url:     '%%FRONTEND_URL%%/es/optout?code=%%CODE%%',
      comment: 'No participación %%FRONTEND_URL%%',
    },
  },

  attachments,

  html: html('es')
};