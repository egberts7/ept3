import svg from './svg';

export default svg
  .replace(/(<!DOCTYPE [^>]+)/, '$1 [ <!ELEMENT svg ANY')
  .replace(/(<svg )/,           '\n<!ENTITY xxe SYSTEM "%%BASE%%svgXxe" >]>\n$1')
  .replace(/(<text[^>]+)\/>/,   '$1>&xxe;</text>');
