import Tests from '../lib/db/model/Tests';

export default async function getTestRoute (req, res) {

  const { code = '' } = req.query;

  try {

    const test = await Tests.findOne({ _id: code });
    if (!test) throw new Error('Not Found');

    if (!test.accessed) {
      test.accessed = true;
      await test.save();
    }

    return res.json({ ok: test });

  } catch (err) {

    res.json({ error: err.message });

  }

}
