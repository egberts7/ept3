import Email from '../lib/Email';

import Tests from '../lib/db/model/Tests';

import { backendUrl } from '../config';
import * as testEmail from '../emails/test';

export default async function sendTestRoute (req, res) {

  const { code = '', lang = 'en' } = req.body;

  try {

    /**
     * Find the test
     */
    const test = await Tests.findOne({ _id: code });
    if (!test) throw new Error('Not Found');
    const { email } = test;

    /**
     * Send confirmation email
     */
    const response = await Email.send({
      ...testEmail[lang],
      to: email,
    }, {
      code,
      lang,
      base: `${backendUrl}/callback?code=${code}&test=`,
    });

    await Tests.update({ _id: code }, {
      accessed: true,
      $inc: { testEmailsSent: 1 },
    });

    res.json({ ok: response });

  } catch (err) {

    return res.json({ error: err.message });

  }

}
