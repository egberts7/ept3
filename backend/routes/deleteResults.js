import Callbacks from '../lib/db/model/Callbacks';

import { newCallback } from '../lib/longPollManager';

export default async function deleteResultsRoute (req, res) {

  const { code = '' } = req.body;

  try {

    await Callbacks.remove({ code });

    res.json({ ok: true });

    newCallback(code);

  } catch (err) {

    res.json({ error: err.message });

  }

}
