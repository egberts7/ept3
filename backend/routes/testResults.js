import Callbacks from '../lib/db/model/Callbacks';

import { sleepUntilCallback } from '../lib/longPollManager';

export default async function testResultsRoute (req, res) {

  const { code = '', since=0, longPoll } = req.query;

  try {

    let callbacks = await Callbacks.find({
      code,
      time: { $gt: new Date(parseInt(since, 10)) },
    }).sort({ time: 1 });

    if (longPoll && callbacks.length === 0) {

      await sleepUntilCallback(code);
      callbacks = await Callbacks.find({
        code,
        time: { $gt: new Date(parseInt(since, 10)) },
      }).sort({ time: 1 });

    }

    return res.json({
      ok: callbacks.map(c => ({
        test: c.test,
        ip:   c.ip,
        time: c.time.getTime(),
        http: c.http,
      })),
    });

  } catch (err) {

    res.json({ error: err.message });

  }

}
