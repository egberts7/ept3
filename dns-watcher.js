#!/usr/bin/env babel-node

import os              from 'os';
import { Tail }        from 'tail';
import { dnsTestZone } from './backend/config';
import API             from './frontend/lib/api';

const queryLogPath = '/var/log/bind/query.log';

const rx = new RegExp(
  `^\\S+ \\S+ .*: client (\\S+?)#\\d+.*?: query: ([a-zA-Z0-9]+)\\.(anchor|link)-test\\.${dnsTestZone.replace(/\./g, '\\.')} IN A(?:AAA)? `,
  'i'
);

// Collect up local IPs so we can exclude them from callbacks
const localIPs = (() => {
  const ifaces = os.networkInterfaces();
  return Object.keys(ifaces).reduce((o, ifname) => {
    ifaces[ifname].forEach(iface => {
      if (!iface.family.match(/^IPv[46]$/)) return;
      o[iface.address.toLowerCase()] = true;
    });
    return o;
  }, {});
})();

const tail = new Tail(queryLogPath);

const cache = {};

tail.on('line', data => {

  // Clear expired cache entries
  Object.keys(cache).forEach(k => {
    if (cache[k] < Date.now() - 30000) delete cache[k];
  });

  const m = data.toLowerCase().match(rx);
  if (!m) return;

  const [ ip, code, label ] = m.slice(1);

  const test = 'dns' + label.substr(0,1).toUpperCase() + label.substr(1);

  if (localIPs[ip.toLowerCase()]) return;

  const cacheKey = [ ip, code, test ].join(' ');
  if ({}.hasOwnProperty.call(cache, cacheKey)) return;
  cache[cacheKey] = Date.now();

  API.get('callback', { ip, code, test })
    .then(() => console.log(`Successfully logged ${test} / ${code} / ${ip}`))
    .catch(e => console.log(`Failed to log ${test} / ${code} / ${ip} - ${e}`));
});
